function promptNumber(question) {

  let result = Number(prompt(question));
  
  // Number.isNaN(result): true if the given value is NaN and its type is Number; otherwise, false.;
  if (Number.isNaN(result)) return null;
  
  else return result;
}
console.log(promptNumber("how many trees do you see?"))