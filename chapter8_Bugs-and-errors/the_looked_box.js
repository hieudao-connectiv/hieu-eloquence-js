const box = {
  locked: true,
  unlock()  {this.locked = false;},
  lock() {this.locked = true;},
  _content: [],
  get content() {
    if (this.locked) throw new Error("Locked!");
    return this._content;
  }
}

function withBoxUnlocked(body) {
  // console.log("1");

  let locked = box.locked;
  // console.log(locked);          // true

  if (!locked) {
    return body();
  }

  box.unlock();                    // locked: false
  try {
    return body();
  } finally {
    box.lock();
    // console.log(locked);           // true
  }
}


// this function runs the first
withBoxUnlocked(() => {
  // console.log("2");

  box.content.push('Hieu');
});


// try...catch runs the second
try {
  withBoxUnlocked(() => {
    // console.log("3");

    throw new Error("Pirates on horizon! A")
  })
} catch (e) {
  // console.log('4');

  console.log("Error raised: ", e);     // Error raised:  Error: Pirates on horizon! A
}

console.log(box.locked);        // true

/////

// const box = {
//   locked: true,
//   unlock() { this.locked = false; },
//   lock() { this.locked = true;  },
//   _content: [],
//   get content() {
//     if (this.locked) throw new Error("Locked!");
//     return this._content;
//   }
// };

// function withBoxUnlocked(body) {
//   let locked = box.locked;
//   if (!locked) {
//     return body();
//   }

//   box.unlock();
//   try {
//     return body();
//   } finally {
//     box.lock();
//   }
// }

// withBoxUnlocked(function() {
//   box.content.push("gold piece");
// });

// try {
//   withBoxUnlocked(function() {
//     throw new Error("Pirates on the horizon! Abort!");
//   });
// } catch (e) {
//   console.log("Error raised:", e);
// }

// console.log(box.locked);
// // → true