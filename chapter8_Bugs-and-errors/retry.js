class MultiplicatorUnitFailure extends Error{}

function primitiveMultiply (a,b) {
  if (Math.random() < 0.2) {
    // if (0.3 < 0.2) {
    return a * b;
  } else {
    throw new MultiplicatorUnitFailure("false");
  }
}

function reliableMultiply (a, b) {
  for (;;) {
    try {
      return primitiveMultiply(a, b);
    } catch (e) {
      if (! (e instanceof MultiplicatorUnitFailure)) {
        // throw e;
        console.error(e) ;
      }
    }
  }
}

console.log(reliableMultiply(3,4));