function numberToString(n, base = 10) {
  let result = "", sign = "";
  if (n < 0) {
    sign = "-";
    n = -n;
  }
  do {
    // String() convert from number to string
    result = String(n % base) + result;
    // console.log(result);

    // n /= base;
    n = Math.floor(n/base);
    // console.log(n);

  } while (n > 0);
  return sign + result;
}
console.log(numberToString(13, 10));    // 13  (string)
console.log(numberToString(-10, 10));   // -10
console.log(numberToString(90, 10));    // 90
console.log(numberToString(45, 10));    // 45