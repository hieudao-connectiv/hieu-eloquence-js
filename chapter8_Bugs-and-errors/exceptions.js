function promptDirection(question) {
  let result = prompt(question);
  if (result.toLowerCase() == 'left') return "L";
  if (result.toLowerCase() == 'right') return "R";
  throw new Error("Invalid direction: " + result);
}

function look() {
  if (promptDirection("Which way? Left or Right?") == 'L') {
    return 'A house';
  } else {
    return 'Two hungry bears';
  }
}

try {
  console.log("you see: ", look());
} catch (error) {
  console.log("Something went wrong: " + error);
}