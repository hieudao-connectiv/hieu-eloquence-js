
// #METHOD 1
// looping will be executed until the valid value was entered, even though you click the cancel button, the loop will continue
// for (;;) {
//   try {
//     let dir = promptDirection("Where? 'Left' or 'Right'?");
//     console.log(dir);
//     break;
//   } catch ( error ) {
//     console.log("Not a valid direction: ", error);
//   }
// }

function promptDirection(question) {
  let result = prompt(question);
  if (result.toLowerCase() == 'left') return "L";
  if (result.toLowerCase() == 'right') return "R";
  throw new Error("Invalid direction: ", result);
}


////////

// #METHOD 2
// The new error class extends Error.
// InputError object behave like Error object, except that they have a different class by which we can recognize them
// the for loop will only execute once
class InputError extends Error{};

for (;;) {
  try {
    let dir = promptDirection("Where? 'Left' or 'Right'?");
    console.log(dir);
    break;
  } catch ( error ) {
    if (error instanceof InputError) {
      console.log("Not a valid direction: ", error);
    } else {
      throw error;
    }
  }
}