"use strict";
// putting the string "use strict" at the top of a file or a function body. It helps you spot a problem.

function canYouSpotTheProblem() {
  "use strict";
  for (counter = 0; counter < 10; counter++) {
    console.log('hello');
  }
}
// canYouSpotTheProblem();     // Uncaught ReferenceError: counter is not defined



function Person(name) { this.name = name; }
let hieu = Person("hieu");
console.log(name);

// doesn't use 'use strict' -> 'hieu'
// use 'use strict'         -> Uncaught TypeError: Cannot set property 'name' of undefined

