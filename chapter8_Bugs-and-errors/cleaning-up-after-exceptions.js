const accounts = {
  a: 100,
  b: 0,
  c: 20
};

function getAccount() {
  let accountName = prompt("Enter the account name is received money: ");
  if (!accounts.hasOwnProperty(accountName)) {
    throw new Error(`No such account: ${accountName}`);
  } c
  return accountName;
}

// function transfer(from, amount) {
//   if (accounts[from] < amount) return;
//   accounts[from] -= amount;
//   accounts[getAccount()] += amount;
// }

// another way is more intelligent

function transfer(from, amount) {
  if ( accounts[from] < amount ) return;
  let progress = 0;
  try {
    accounts[from] -= amount;
    progress = 1;

    accounts[getAccount()] += amount;
    progress = 2;
  } catch (error) {
    console.error("Have an error: ", error);
  } finally {
    if (progress == 1) {
      accounts[from] += amount;
    }
  }
}

transfer("a", 20);
console.log(accounts);
