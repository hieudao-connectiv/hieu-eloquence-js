function test (label, body) {
  if (!body()) console.log(`Failed: ${label}`);
}

test('convert Latin text to uppercase', () => {
  return 'hello'.toUpperCase() == "HeLLO";
})
// Failed: convert Latin text to uppercase