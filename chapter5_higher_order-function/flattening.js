function flattening (array) {
  return array.reduce((total, currentValue) => total.concat(currentValue), []);
}

console.log(flattening([[1,2,3], [4,5], [6]]));