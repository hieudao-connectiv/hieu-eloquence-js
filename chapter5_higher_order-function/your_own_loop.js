function own_loop(value, test, update, body) {
  for (let n = value; test(n) ; n = update(n)) {
    body(n);
  }
}
own_loop(4, n => n > 0, n => n -1, console.log);