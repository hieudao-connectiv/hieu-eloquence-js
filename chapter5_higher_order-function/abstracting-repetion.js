let labels = [];
function repetition(n, action) {
  for (let i = 0; i < n; i++) {
    action(i);
  }
  return labels;
}
console.log(repetition(5, i => {
  labels.push(`Unit ${i + 1}`)
}));

//

function noisy(f) {
  return (...args) => {
    console.log("Calling with", args);
    let result = f(...args);
    console.log("called with", args, ", returned", result);
    return result;
  }
}
noisy(Math.min)(3,2,1);
// Calling with (3) [3, 2, 1] (line 14)
// called with (3) [3, 2, 1] , returned 1 (line 16)

//

function repeat(n, action) {
  for (let i = 0; i < n; i++) {
    action(i)
  }
}

function unless(test, then) {
  if (!test) then();
}

repeat(3, n => {
  unless(n % 2 == 1, () => {
    console.log(n, "is even")
  })
})
// 0 is even
// 2 is even