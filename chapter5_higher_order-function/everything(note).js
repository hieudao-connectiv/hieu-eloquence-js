function loop_for_every(array, predicateF) {
  for (ele of array) {
    if (!predicateF(ele)) {
      return false
    }
  }
  return true;
}

function loop_some_every(array, predicateF) {
  return !array.some(element => !predicateF(element));  // note
}

console.log(loop_for_every([1,2,7], n => n < 10));    // true
console.log(loop_for_every([1,12,7], n => n < 10));   // false
console.log(loop_for_every([], n => n < 10));         // true

console.log(loop_some_every([1,2,7], n => n < 10));   // true
console.log(loop_some_every([1,12,7], n => n < 10));  // false
console.log(loop_some_every([], n => n < 10));        // true

//                                           (n < 15)
// (n)   1          2          16         20
//       T          T           F         F       
//   (!T = F)   (!T = F)    (!F = T)   (!F = T)   -> T = !F -> !(!F) = T
//
//       1          2          3           4
//       T          T          T           T    
//   (!T = F)   (!T = F)    (!T = F)   (!T = F)   -> F = !T -> !(!T) = F
//
//       20         21         22        23
//       F          F          F         F 
//   (!F = T)   (!F = T)    (!F = T)   (!F = T)   -> T = !F -> !(!F) = T
