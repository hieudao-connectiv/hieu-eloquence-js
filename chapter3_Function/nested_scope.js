// Nested Scope
// Blocks and functions can be created inside other blocks and functions, producing multiple degrees of locality

const hummus = function (factor) {
  const ingredient = function (amount, unit, name) {
    let ingredientAmount = amount * factor;
    if (ingredientAmount > 1) {
      unit += "s";
    }
    console.log(`${ingredientAmount} ${unit} ${name}`);
  }

  ingredient(1, 'can', 'chickpeas');
  ingredient(0.25, 'cup', 'tahini');
  ingredient(1.55, 'cup', 'lemon juice');
  ingredient(2, 'clove', 'garlic');
}

hummus(3); // 3 cans chickpeas, 0.75 cup tahini, 4.65 cups lemon juice, 6 cloves garlic

