function printFarmInventory (cows, chickens) {
  let cowString = String(cows);
  while (cowString.length < 3){
    cowString = "0" + cowString;
  }
  console.log(`${cowString} cows`);

  let chickensString = String(chickens);
  while (chickensString.length < 3) {
    chickensString = "0" + chickensString;
  }
  console.log(`${chickensString} chickens`);
}
printFarmInventory(3,80);


//


function zeroPad (number, width) {
  let string = String(number);
  while (string.length < width) {
    string = "0" + string;
  }
  return string;
}

function printFarmInventory(cows, chickens, pigs) {
  console.log(`${zeroPad(cows, 3)} cows`);
  console.log(`${zeroPad(chickens, 3)} chickens`);
  console.log(`${zeroPad(pigs, 3)} pigs`);
}

printFarmInventory(2,3,4); // 002 cows, 003 chickens, 004 pigs