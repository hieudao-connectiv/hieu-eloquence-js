// Closure 
// A closure is a function having access to the parent scope, even after the parent function has closed.

// Example 1
function makeAdder(x) {
  return function (y) {
    return x + y;
  }
}
let makeAdder1 = makeAdder(1); // makeAdder1 is a closure
let makeAdder2 = makeAdder(2); // makeAdder2 is a closure

console.log(makeAdder1(1)); // 2
console.log(makeAdder2(2)); // 4

// Example 2 
// The self-invoking function only runs once (from reloading page). It sets the counter to zero (0), and returns a function expression.

// Function closures (w3school)
// Look at this, 'add' variable becomes a function. It could be access the counter in the parent scope. This is called a Javascript "Closure".

var add = (function () {
  var counter = 0;
  return function () { counter += 1; return counter}
})();

console.log(add()); // 1
console.log(add()); // 2
console.log(add()); // 3
console.log(add()); // 4

