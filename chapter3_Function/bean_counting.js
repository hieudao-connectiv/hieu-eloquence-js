var bean_counting = function (string, text) {
  var i = 0;
  var character = "";
  var countUppercase = 0;
  var countNumeric =  0;
  var count_similar_Text = 0;
  while (i < string.length) {
    character = string.charAt(i);
    if (!isNaN (character * 1)) {
      countNumeric++;
    } else if (character == character.toUpperCase()){
      countUppercase++;
      if (character === text) {
        count_similar_Text++;
      }
    } else {
      if (character === text) {
        count_similar_Text++;
      }
    }
    i++;
  }
  return `With "${string}" string \nwe have ${countUppercase} characters are uppercase characters \n${countNumeric} characters are numeric \n${count_similar_Text} characters are similar with "${text}" character`;
}

console.log(bean_counting("dao34TrungHieu", 'u'));

