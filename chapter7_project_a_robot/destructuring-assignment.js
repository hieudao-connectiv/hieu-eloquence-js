
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment

// Distructuring
// Array 
[a, b] = [10, 20];
console.log(a);             // 10
console.log(b);             // 20

[a, b, ...rest] = [1, 2, 3, 4, 5, 6];
console.log(a);             // 1
console.log(b);             // 2
console.log(rest);          // (4) [3, 4, 5, 6]

var a, b, rest;
({ a, b } = { a: 10, b: 20 });
console.log(a);             // 10
console.log(b);             // 20

({ a, b, ...rest } = { a: 10, b: 20, c: 30, d: 40 });
console.log(rest);          // {c: 30, d: 40}

var x = [2, 3, 4, 5];
var [y, z] = x;
console.log(y);             // 2
console.log(z);             // 3

// 'use strict'
var foo = ['one', 'two', 'three'];
var [one, two, three] = foo;
console.log(one)            // one
console.log(two)            // two
console.log(three)          // three

var a, b;
[a = 2, b = 0] = [1];
console.log(a);             // 1
console.log(b);             // 0

var a = 1;
var b = 3;
[a, b] = [3, 1];
console.log(a);             // 3
console.log(b);             // 1


function f() {
  return [1, 2];
}

var [a, b] = f();
console.log(a);             // 1
console.log(b);             // 2


function nude() {
  return [1, 5, 3];
}
var [a, , b] = nude();
console.log(a);             // 1
console.log(b);             // 3  

var [a, ...b] = [1, 2, 3, 4];
console.log(a);             // 1
console.log(b);             // (3) [2, 3, 4]

var o = { p: 42, q: true };
var { p, q } = o;
console.log(p);             // 42
console.log(q);             // true

var a, b;
({ a, b } = { a: 1, b: 2 }); 
console.log(a);             // 1
console.log(b);             // 2

var o = { p: 42, q: true };
var { p: foo, q: girl } = o;
console.log(foo);           // 42
console.log(girl);          // true

var { a = 10, b = 4 } = { a: 3 };
console.log(a);             // 3
console.log(b);             // 4

({ a: aa = 10, b: bb = 11 } = { a: 2 });
console.log(aa);            // 2
console.log(bb);            // 11

({ a, c: { e } } = { a: 2, b: 1, c: { e: 1, f: 2 } });
console.log(a);             // 2
console.log(e);             // 1


var obj = {
  id: 1,
  displayname: 'Hieu',
  fullname: {
    firstname: 'Dao',
    lastname: 'Hieu'
  }
}
function user({ id }) {
  return id;
}
function nameUser({ displayname, fullname: { lastname: name } }) {
  return `${displayname} is ${name}`;
}
console.log(user(obj));             // 1
console.log(nameUser(obj));         // Hieu is Hieu

function check({ a = 'aa', b = { c: 3, d: 1 }, e = 2 } = {}) {

  // function check({a = 'aa', b = {c: 3, d: 1}, e = 2}) {   // cách này có thể rút gọn ở đây nhưng khi gọi thì lại phải cung cấp ít nhất 1 đối số. Nên sẽ không tiện cho việc gọi hàm.
  console.log(a, e, b.c);           
}

check();                                  // aa, 2, 3
check({ b: { c: 'p', d: 3 }, e: 2 });     // aa, 2, p

const metadata = {
  title: 'hieu',
  translations: [
    {
      locale: 'Hanoi',
      localization_tags: [],
      last_edit: '2019-08-05',
      url: 'de/docs/tools/scratchpad',
      title: 'Javascript-Umgebung'
    }
  ],
  url: '/en-US/docs/tools/scratchpad'
};

let {
  title: englishTitle,
  translations: [
    {
      title: localTitle,
    },
  ],
} = metadata;

console.log(englishTitle);          // hieu
console.log(localTitle);            // Javascript-Umgebung


var people = [
  {
    name: 'Mike Smit',
    family: {
      mother: 'Jane Smith',
      father: 'Harry Smith',
      sister: 'Samantha Smith'
    },
    age: 35
  },
  {
    name: 'Tom Jones',
    family: {
      mother: 'Norah Jones',
      father: 'Richard Jones',
      brother: 'Howard Jones'
    },
    age: 25
  }
];

for (var { name: n, family: { father: f } } of people) {
  console.log('Name: ' + n + ', Father: ' + f);
}
// Name: Mike Smit, Father: Harry Smith
// Name: Tom Jones, Father: Richard Jones


let key = 'z';
({ [key]: foo } = { z: 'bar' });
// phải để từ khóa key ở dạng array vì chỉ ở array mới có khả năng cho gán tên biến

console.log(foo);                     // bar

const hieu = { 'contrau': true };
const { 'contrau': tau } = hieu;
console.log(tau);                     // true


const fullName = [
  { id: 1, name: 'hieu' },
  { id: 2, name: 'trung' },
  { id: 3, name: 'dao' }
];
const [, , { name }] = fullName;
console.log(name);                    // dao


const status = {
  max: 56.78,
  standard_deviation: 4.34,
  median: 34.54,
  mode: 23.87,
  min: -0.75,
  average: 35.85
};
const half = (function () {
  return function half({ max, min }) {
    console.log((max + min) / 3);
  };
})();
half(status);                          // 18.676666666666666


// Write concise

// write concise object literal declarations using simple fields   
const getMousePosition = (x, y) => ({ x, y });
// const getMousePosition2 = (x, y) => {
//   return {
//     x: x,
//     y: y
//   }
// };
console.log(getMousePosition(1, 2))     // {x: 1, y: 2};

// write concise declarative functions
const bicycle = {
  gear: 2,

  // setGear: function(newGear) {
  setGear(newGear) {
    this.gear = newGear;
  }
}

console.log(bicycle.gear);              // 2
  
bicycle.setGear('Filco')
console.log(bicycle.gear);              // Filco
