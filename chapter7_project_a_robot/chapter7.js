const roads = [
  "Alice's House-Bob's House", "Alice's House-Cabin", "Alice's House-Post Office", "Bob's House-Town Hall", "Daria's House-Ernie's House", "Daria's House-Town Hall", "Ernie's House-Grete's House", "Grete's House-Farm", "Grete's House-Shop", "Marketplace-Farm", "Marketplace-Post Office", "Marketplace-Shop", "Marketplace-Town Hall", "Shop-Town Hall"
]


///    BÃI CỎ 

console.log(roads.map(r => r.split('-')));
// ⯆ (14) [Array(2), Array(2), Array(2), Array(2), Array(2), Array(2), Array(2), Array(2), Array(2), Array(2), Array(2), Array(2), Array(2), Array(2)]
//    ▶ 0: (2) ["Alice's House", "Bob's House"]
//    ▶ 1: (2) ["Alice's House", "Cabin"]
//    ▶ 2: (2) ["Alice's House", "Post Office"]
//    ▶ 3: (2) ["Bob's House", "Town Hall"]
//    ▶ 4: (2) ["Daria's House", "Ernie's House"]
//    ▶ 5: (2) ["Daria's House", "Town Hall"]
//    ▶ 6: (2) ["Ernie's House", "Grete's House"]
//    ▶ 7: (2) ["Grete's House", "Farm"]
//    ▶ 8: (2) ["Grete's House", "Shop"]
//    ▶ 9: (2) ["Marketplace", "Farm"]
//    ▶ 10: (2) ["Marketplace", "Post Office"]
//    ▶ 11: (2) ["Marketplace", "Shop"]
//    ▶ 12: (2) ["Marketplace", "Town Hall"]
//    ▶ 13: (2) ["Shop", "Town Hall"]
//      length: 14
//    ▶ __proto__: Array(0)


function buildGraph(edges){
  let graph = Object.create(null);

  function addEdge(from, to) {

    // object['value'] == object[key]
    if (graph[from] == null) {
      graph[from] = [to];
    } else {
      graph[from].push(to);
    }
  }

  for (let [from, to] of edges.map(r => r.split('-'))) {
    addEdge(from, to);
    addEdge(to, from);
  }
  return graph;
}

const roadGraph = buildGraph(roads);
console.log(roadGraph);
// console.log(roadGraph['Post Office']);
// ⯆ {Alice's House: Array(3), Bob's House: Array(2), Cabin: Array(1), Post Office: Array(2), Town Hall: Array(4), …}
//    ▶ Alice's House: (3) ["Bob's House", "Cabin", "Post Office"]
//    ▶ Bob's House: (2) ["Alice's House", "Town Hall"]
//    ▶ Cabin: ["Alice's House"]
//    ▶ Daria's House: (2) ["Ernie's House", "Town Hall"]
//    ▶ Ernie's House: (2) ["Daria's House", "Grete's House"]
//    ▶ Farm: (2) ["Grete's House", "Marketplace"]
//    ▶ Grete's House: (3) ["Ernie's House", "Farm", "Shop"]
//    ▶ Marketplace: (4) ["Farm", "Post Office", "Shop", "Town Hall"]
//    ▶ Post Office: (2) ["Alice's House", "Marketplace"]
//    ▶ Shop: (3) ["Grete's House", "Marketplace", "Town Hall"]
//    ▶ Town Hall: (4) ["Bob's House", "Daria's House", "Marketplace", "Shop"]


////     THE TASK
class VillageState {
  constructor(place, parcels) {
    this.place = place;
    this.parcels = parcels;
  }

  move(destination) {

    // to check if the departure point (place) belongs to 'roadGraph' and includes 'destination'  
    if (roadGraph[this.place].includes(destination)) {
      return this;          // point to the 'first' object
    } else {

      // Filter the valid value. This program runs from map() to filter()
      let parcels = this.parcels.map(p => {
        if (p.place != this.place) return p;

        // return the valid parcel
        return {place: destination, address: p.address};

      // Check whether the place and the address is the same
      }).filter(p => p.place != p.address);

      return new VillageState(destination, parcels);
    }

  }

}

let first = new VillageState (
  "Post Office", [{place: "Post Office", address: "Alice's House"}]
)
let next = first.move("Alice's House");

console.log(next.place);            // Alice's House
console.log(next.parcels);          // []                 // don't understand
console.log(first.place);           // Post Office


///////////////// SIMULATION


function runRobot(state, robot, memory) {
  for (let turn = 0; ; turn++) {
    if (state.parcels.length == 0) {
      console.log(`Done in ${turn} turns`);
      break;
    }
    let action = robot(state, memory);
    state = state.move(action.direction);
    memory = action.memory;
    console.log(`Moved to ${action.direction}`);
  }
}

function randomPick(array) {
  let choice = Math.floor(Math.random() * array.length);
  return array[choice];
}

function randomRobot(state) {
  return {direction: randomPick(roadGraph[state.place])}
}

VillageState.random = function(parcelCount = 5) {
  let parcels = [];
  for (let i = 0; i < parcelCount; i++) {
    let address = randomPick(Object.keys(roadGraph));
    let place;
    do {
      place = randomPick(Object.keys(roadGraph));
    } while (place == address);
    parcels.push({place, address});
  }
  return new VillageState("Post Office", parcels);
}

// runRobot(VillageState.random(), randomRobot);
// Moved to Marketplace
// Moved to Town Hall
// ...
// Done in about 63 turns


////////////////////  THE MAIL TRUCK'S ROUTE

const mailRoute = [
  "Alice's House", "Cabin", "Alice's House", "Bob's House", "Town Hall", "Daria's House", "Ernie's House", "Grete's House", "Shop", "Grete's House", "Farm", "Marketplace", "Post Office"
]

function routeRobot (state, memory) {
  if (memory.length == 0) {
    memory = mailRoute;
  }
  return {direction: memory[0], memory: memory.slice(1)};
}


///////////////////    PATHFINDING

function findRoute(graph, from, to) {
  let work = [{at: from, route: []}];
  for (let i = 0; i < work.length; i++) {
    let {at, route} = work[i];
    for (let place of graph[at]) {
      if (place == to) return route.concat(place);
      if (!work.some(w => w.at == place)) {
        work.push({at: place, route: route.concat(place)});
      }
    }
  }
}

function goalOrientedRobot({place, parcels}, route) {
  if (route.length == 0) {
    let parcel = parcels[0];
    if (parcel.place != place) {
      route = findRoute(roadGraph, place, parcel.place);
    } else {
      route = findRoute(roadGraph, place, parcel.address);
    }
  }
  return {direction: route[0], memory: route.slice(1)};
}