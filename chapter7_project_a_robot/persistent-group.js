class PGroup {
  constructor(members) {
    this.members = members;
  }

  add(value) {
    if (this.has(value)) return this;
    return new PGroup(this.members.concat([value]));
  }

  delete(value) {
    if (!this.has(value)) return this;
    return new PGroup(this.members.filter(m => m !== value));
  }

  has(value) {
    return this.members.includes(value);
  }

}

PGroup.empty = new PGroup([]);
// console.log(PGroup.empty);
// ⯆ PGroup {members: Array(0)}
//    ▶ members: []
//    ▶ __proto__: Object

let a = PGroup.empty.add("a");
console.log(a);
// PGroup {members: Array(1)}
// members: ["a"]
// __proto__: Object

let ab = a.add("b");
console.log(ab);
// ⯆ PGroup {members: Array(2)}
//    ▶ members: Array(2)
//        0: "a"
//        1: "b"
//        length: 2
//      ▶ __proto__: Array(0)
//  ▶ __proto__: Object

let b = ab.delete('a');
console.log(b);
// ⯆ PGroup {members: Array(1)}
//    ▶ members: ["b"]
//    ▶ __proto__: Object