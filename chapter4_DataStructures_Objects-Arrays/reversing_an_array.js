let reverseArray = function (array) {
  var newArray = [];
  for (let i = 0; i < array.length; i++) {
    newArray.unshift(array[i])
  }
  return newArray;
}

console.log(reverseArray([1,2,3,4]));   // [4,3,2,1]

