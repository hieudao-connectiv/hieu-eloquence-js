let range = function (a,b) {
  let arrayRange = [];
  for (var i = a; i <= b; i++) {
    arrayRange.push(i);
  }
  return arrayRange;
}
console.log((range(1,10)));   // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

//

let sum = function (array) {
  var sum = 0;
  for (var i = 0; i < array.length; i++) {
    sum += array[i];
  }
  return sum;
}
var array = [1,2,3,4,5];
console.log(sum(array));    // 15

//

let modifyRange = function (a,b, c = 1) {
  let array = [];

  if (c > 0) {

    for (let i = a; i <= b; i += c) {
      array.push(i);
    }
  } else if (c < 0) {

    for (let i = b; i >= a; i += c) {
      array.push(i);
    }
  }

  return array;
}
console.log(modifyRange(1, 10, 2));   // [1, 3, 5, 7, 9]
console.log(modifyRange(1, 10));      // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(modifyRange(1, 10, -2));      // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]