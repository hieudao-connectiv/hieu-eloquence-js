console.log('car and cat'.match(/ca[tr]/g));
// (2) ["car", "cat"]

console.log('pop and prop'.match(/pr?op/g));
// (2) ["pop", "prop"]

console.log('ferret, ferry, and ferrari'.match(/ferr[et|y|ari]/g));
// (3) ["ferre", "ferry", "ferra"]

console.log('ferret, ferry, and ferrari'.match(/ferr(et|y|ari)/g));
// (3) ["ferret", "ferry", "ferrari"]

console.log('any word ending in ious'.match(/ious\b/g));
// ["ious"]

console.log('A whitespace character followed by a period ,comma ,colon :or ;semicolon'.match(/\s[.,:;]/g));
// (4) [" ,", " ,", " :", " ;"]

console.log('A word longer than six letters'.match(/\w{7,}/g));
// ["letters"]

console.log('A word without the letter e (or E)'.match(/\b[^\We]+\b/gi));   
// (4) ["A", "word", "without", "or"]