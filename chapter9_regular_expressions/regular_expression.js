
// Creating a regular expression
// ===============
// A regular expression is a type of object
let re1 = new RegExp("abc");
let re2 = /abc/;



// Testing for matches
//  ===============
// The 'test()' method: tests for a match in a string, return true or false
console.log(/abc/.test("abced"))        // true
console.log(/abc/.test("abxed"))        // false: because the characters don't follow the ordered alphabet



// Set of characters
// ===============
// In a regular expression, putting a set any of the characters between 'square brackets' that part of the expression match any of characters between the brackets

// Both of the following expressions match all strings that contain a digit
console.log(/[0123456789]/.test('hieudao 868'));              // true
console.log(/[0-9]/.test('hieudao 868'));                     // true

// \d: any digit character
// \w: any alphanumeric character ("word character")
// \s: any whitespace character (space, tab, newline, and similar)
// \D: A character isn't a digit
// \W: A non-alphanumeric word
// \S: A non-whitespace character
// . any character excepts for newline

let dateTime = /\d\d-\d\d-\d\d\d\d \d\d:\d\d/;
console.log(dateTime.test("12-03-2020 12:53"));               // true
console.log(dateTime.test("12-March-2020 12:53"));            // false

let notBinary = /[^01]/;          
// '^' a negative sign (một dấu phủ định). Mean: doesn't match with all the characters in the string
console.log(notBinary.test("110010101000101010"));        // false: because the string match all character in the string
console.log(notBinary.test("110010101300101010"));        // true: because the string doesn't match all character in the string



// Repeating part of a string
// ===============
// The plus sign (+) after something in regular expression indicating that the element can be repeated more than once. 
// Thus, /\d+/ matches one or more digit characters

// The star sign (*) is the similar meaning as the plus sign (+), but also allows the patten to match zero times
console.log(/'\d+'/.test("'123'"));           // true
console.log(/'\d+'/.exec("'123'"));           // ["'123'", index: 0, input: "'123'", groups: undefined]
console.log(/'\d+'/.test("''"));              // false
console.log(/'\d*'/.test("'123'"));           // true
console.log(/'\d*'/.test("''"));              // true

// A question mark (?) makes the element standing before it occur zero times or one time
console.log(/neighbo?r/.test('neighbor'));        // true
console.log(/neighbo?r/.test('neighbr'));         // true

// Putting {4} after an element, requires it to occur exactly four times
// Putting a range {2,4} means the element must occur at lest two times or at most four times
// Or when you omit the second parameter, for example as {5,}, that means five or more times
let newDateTime = /\d{1,2}-\d{1,2}-\d{4} \d{1,2}:\d{1,2}/;
console.log(newDateTime.test("1-30-2020 2:23"));              // true



// Grouping sub-expression
// ===============
// To use an operator like * or + on more than one element at a time, you have to use parentheses
// 'i' starts case insensitive mode (chế độ không phân biệt chữ hoa chữ thường); '-i' turns off case-insensitive
// 'i' allows to match with the uppercase H in the input string.
// A repeated capturing group will only capture the last iteration

let cartoonCrying = /boo+(hoo+)+/i;
console.log(cartoonCrying.test("booooohoohooooHooo"));     // true

// The first and second + characters apply only to the second 'o' in 'boo' and 'hoo' respectively. The third + character applies to the whole group (hoo+), matching one or more sequences like that



// Matches and groups
// ===============
// Regular expression have an exec() method will return 'null' if no match was found and return an object with information about the match otherwise

let match = /\d+/.exec('one two 100');
console.log(match);
// ⯆ ["100", index: 8, input: "one two 100", groups: undefined]
//     0: "100"
//     groups: undefined
//     index: 8
//     input: "one two 100"
//     length: 1
//   ▶ __proto__: Array(0)

console.log(match.index);         // 8: tell us where in the string the successful match begins

// String values have a 'match' method that behaves similarly
console.log('one two three 100'.match(/\d+/));
// ⯆ ["100", index: 14, input: "one two three 100", groups: undefined]
//     0: "100"
//     groups: undefined
//     index: 14
//     input: "one two three 100"
//     length: 1
//   ▶ __proto__: Array(0)

let quotedText = /'([^']*)'/;
// " ' " matches the character ' literally (case insensitive)
// chọn những phần tử trong khoảng ([]) từ dấu ' nhưng không kèm dấu ' (^') và lấy nhiều lần (*) ==> hello
// Rồi chọn tiếp 'group' (dấu ngoặc đơn) phần tử trong chuỗi ( ' ) thỏa mãn ở vế trước đấy ==> ('hello')

console.log(quotedText.exec("she said 'hello'"));
// (2) ["'hello'", "hello", index: 9, input: "she said 'hello'", groups: undefined]

let matchText = /([^e]*)/;
// bắt đầu từ đầu chuỗi, lấy tất cả các phần tử trừ 'e' ra => "sh", "" said 'h", "llo'"
console.log(matchText.exec("she said 'hello'"));
// ["sh", "sh", index: 0, input: "she said 'hello'", groups: undefined]


// When a group doesn't end up being matched at all (as when followed by a question mark), its position in the output array will hold 'undefined'
// Similarly, when a group is matched multiple times, only the last match ends up in the array
console.log(/bad(ly)?/.exec('badly'));      
// ["badly", "ly"] / ["badly", "ly", index: 0, input: "badly", groups: undefined]

console.log(/bad(ly)?/.exec('bad'));        
// ["bad", undefined] / (2) ["bad", undefined, index: 0, input: "bad", groups: undefined]

console.log(/(\d)+/.exec('123'));           
// ["123", "3", index: 0, input: "123", groups: undefined]



// The Date Class
// ==============

// Javascript has a standard class for representing dates called 'Date'
console.log(new Date());        
// --> Sat Dec 26 2020 11:28:54 GMT+0700 (Indochina Time)

// You can create an object for a specific time
console.log(new Date(2020, 12, 26));
// --> Tue Jan 26 2021 00:00:00 GMT+0700 (Indochina Time)
console.log(new Date(2020, 12, 26, 11, 28, 54));
// --> Tue Jan 26 2021 11:28:54 GMT+0700 (Indochina Time)
console.log(new Date().getTime());    // 1608957600137


// The _ (underscore) binding is ignored and only used to skip the full match element in the array returned by exec
function getData(string) {
  let [x, month, day, year] = /(\d{1,2})-(\d{1,2})-(\d{4})/.exec(string);

  console.log(/(\d{1,2})-(\d{1,2})-(\d{4})/.exec(string));
  // ⯆ (4) ["1-29-2020", "1", "29", "2020", index: 0, input: "1-29-2020", groups: undefined]
  //     0: "1-29-2020"
  //     1: "1"
  //     2: "29"
  //     3: "2020"
  //     groups: undefined
  //     index: 0
  //     input: "1-29-2020"
  //     length: 4
  //   ▶ __proto__: Array(0)

  console.log(x);       // '1-29-2020'

  // the month is defined from 0 to 11 so with January you need to subtract 1 to equal 0 (is January in Date of Js)
  return new Date(year, month-1, day);
}
console.log(getData("1-29-2020"));



// Word and string boundaries
// ===============

// If we want to force that the match must span the whole string, we can add the marker's ^ and $. The caret (^) matches the start of a string, whereas the dollar sign matches the end.

// Example: /^\d+$/ matches a whole string of one or more digits.
console.log(/^\d+$/.test('123333'));          // true
console.log(/^\d+$/.test('123d33'));          // false
console.log(/^\d+$/.test('123 33'));          // false

// ^ character matches with the start character of a string. 
// Example: /^!/ matches 'any string that starts' with an exclamation mark(!).
console.log(/^!/.test('!abcd'));             // true
console.log(/^!/.test('!abc 3d'));           // true
console.log(/^!/.test('!123 sad'));          // true
console.log(/^!/.test('abcd'));              // false
console.log(/^!/.test('abc 3d'));            // false
console.log(/^!/.test('123 sad'));           // false

// Ex: ^a will match with the 'a' character of the 'abc' string
console.log(/^a/.exec('abcd 12'));           // ['a']
console.log(/^a/.exec('bacd 12'));           // null

// Ex: ^\w+ will match with the first word ('The') of the "The quick brown fox jumps over the lazy dog" string 
console.log(/^\w+/.exec('abcd 12'));          // ['abcd']
console.log(/^\w+/.exec('1abcd 12'));         // [1abcd]
console.log(/^\w+/.exec('!abcd 12'));         // null

// /x^/ doesn't match any string (there cannot be an x before the start of the string)
console.log(/x^/.exec('abcd 12'));            // null
console.log(/x^/.exec('12'));                 // null

// \b matches with the whole character standing before it
console.log(/\bcat/.exec('my pinkcat is'))          // null
console.log(/\bcat/.exec('my pink cat is'))         // ['cat']
console.log(/\bcat/.exec('my pink cats is'))        // ['cat']
// \b matches with the whole character standing separately
console.log(/\bcat\b/.exec('my pinkcat is'))        // null
console.log(/\bcat\b/.exec('my pink cat is'))       // ['cat']
console.log(/\bcat\b/.exec('my pink cats is'))      // null

// \b matches with the whole character standing after it
console.log(/cat\b/.exec('my pinkcat is'))          // ['cat']
console.log(/cat\b/.exec('my pink cat is'))         // ['cat']
console.log(/cat\b/.exec('my pink cats is'))        // null

// \B opposites with \b
// \B only matches with a part of the string standing before it
console.log(/\Bcat/.exec('my pink cat is'))         // null
console.log(/\Bcat/.exec('my pinkcat is'))          // ['cat']
console.log(/\Bcat/.exec('my pinkcatis'))           // ['cat']

// \B only matches with a part of the string before before it
console.log(/cat\B/.exec('my pink cat is'))         // null
console.log(/cat\B/.exec('my pinkcat is'))          // null
console.log(/cat\B/.exec('my pinkcatis'))           // ['cat']

// \B only matches with a part of the string standing alone
console.log(/\Bcat\B/.exec('my pink cat is'))         // null
console.log(/\Bcat\B/.exec('my pinkcat is'))          // null
console.log(/\Bcat\B/.exec('my pinkcatis'))           // ['cat']



// Choice patterns
// ==============
// The pipe character (|) denotes a selection one out of options
let animalCount = /\b\d+ (pig|cow|chicken)s?\b/;
console.log(animalCount.test('12 pigs'));       // true
console.log(animalCount.test('pig s'));         // false
console.log(animalCount.test('1 cow'));         // true
console.log(animalCount.test('1  cow'));        // false : 2 spaces



// The replace method
// ===============


// The first match was replaced and other cases aren't
console.log("Bororburs".replace(/[ou]/, "a"));          // Barorburs

// When a 'g' option (for global) is added to the regular expression, all matches in the string will be replaced, not just the first
console.log("Bororburs".replace(/[ou]/g, "a"));         // Bararbars

// The $1 and $2 in the placement string refer to 'the parenthesized groups' in the pattern. 
// $1 is replaced by the text that matched against the first group
// $2 is replaced by the text that matched against the second group. And so on, up to $9
// read more: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/n

console.log("Liskov, Barbara \nMcCarthy, John\nWadler, Philip".replace(/(\w+), (\w+)/g, "$2 $1"));
// Barbara Liskov 
// John McCarthy
// Philip Wadler


// It is possible to pass a function instate of a string as an second argument to replace. For each replace, the function will be called with matched groups (as well as the whole match) as arguments, and it return value will be inserted to the new string.
let s = 'the cia and fbi';
console.log(s.replace(/\b(cia|fbi)/g, x => x.toUpperCase()));       // the CIA and FBI

let stock = "1 lemon, 2 cabbages, and 101 eggs";
function minusOne(match, amount, unit) {

  // console.log(match);
  // 1 lemon
  // 2 cabbages
  // 101 eggs

  amount = Number(amount) - 1;
  if (amount == 1) {
    unit = unit.slice(0, unit.length - 1);
  } else if (amount == 0) {
    amount = 'no';
  }
  return amount + " " + unit;
}
console.log(stock.replace(/(\d+) (\w+)/g, minusOne));
// no lemon, 1 cabbage, and 100 eggs



// Greed
// ============
function stripComments(code) {
  return code.replace(/\/\/.*|\/\*[^]*\*\//g,"");           
  //        \/ matches the character / literally 
  //        \* matches the character / literally 

}
console.log(stripComments("1 + /* 2 */3"));              // 1 + 3
console.log(stripComments('x=10;// ten!'));              // x=10;
console.log(stripComments('1 /* a */+/* b */ 1'));       // 1  1



// Dynamically creating RegExp objects
// =============
let name = 'harry';
let text = 'Harry is a suspicious character';
let regexp = new RegExp("\\b(" + name + ")\\b", "gi");            //    \bharry\b
console.log(text.replace(regexp, "_$1_"));
// _Harry_ is a suspicious character

// When creating the '\b' boundary markers, we have to use 'two backslashes' because we are writing them in a normal string, not a slash-enclosed regular expression

let names = "dea+hl[]rd";
let texts = "This dea+hl[]rd guy is super annoying.";
let escape = names.replace(/[\\[.+*?(){|^$]/g, "\\$&");
let regexpp = new RegExp("\\b" + escape + "\\b", "gi");
console.log(texts.replace(regexpp, "_$&_"));
// This _dea+hl[]rd_ guy is super annoying.



// The search method
// ==============
console.log("  word".search(/\S/));     // 2
console.log("  ".search(/\S/));         // -1



// The lastIndex property
let pattern = /y/g;
pattern.lastIndex = 3;
let matched = pattern.exec('xyzzy');
console.log(matched);                     // ["y", index: 4, input: "xyzzy", groups: undefined]
console.log(matched.index);               // 4

// If the match was successful, the call to exec automatically updates the lastIndex property to point after the match. If no match was found, lastIndex is set back to zero.
console.log(pattern.lastIndex);           // 5   

let global = /abc/g;
console.log(global.exec('xyz abc'));      // ["abc", index: 4, input: "xyz abc", groups: undefined]

let stick = /abc/y;
console.log(stick.exec('xyz abc'));       // null


// When using a shared regular expression value for multiple exec calls, these automatically updates to the lastIndex property can cause problems. Your regular expression might be accidentally staring at an index that was left over (còn lại) from a previous call.
let digit = /\d/g;
console.log(digit.exec('here it is: 1'));               
// ["1", index: 12, input: "here it is: 1", groups: undefined]

console.log(digit.exec('This is my first time 1'));    
// ["1", index: 22, input: "This is my first time 1", groups: undefined]

console.log(digit.exec('and now: 1'));        // null

// The match method will find all matches of the pattern in a string and returns an array containing the matched string
console.log("Banana".match(/an/g));           // (2) ["an", "an"]



// Looping over matches
// ================
let input = "A string with 3 numbers in it... 42 and 88.";
let number = /\b\d+\b/g;
let matchesNum;

// console.log(number.exec(input));          
// ["3", index: 14, input: "A string with 3 numbers in it... 42 and 88.", groups: undefined]

// console.log(number.exec(input));          
// ["42", index: 33, input: "A string with 3 numbers in it... 42 and 88.", groups: undefined]

// console.log(number.exec(input));
// ["88", index: 40, input: "A string with 3 numbers in it... 42 and 88.", groups: undefined]

while (matchesNum = number.exec(input)) {
  console.log("Found", matchesNum[0], "at index:", matchesNum.index);
}
// Found 3 at index: 14
// Found 42 at index: 33
// Found 88 at index: 40



// Parsing an ini file
//================
function parseINI(string) {

  // Star with an object to hold the top-level fields
  let result = {};
  let section = result;
  string.split(/\r?\n/).forEach(line => {                 // \r?\n: split in a way that allows both '\n\ and '\r\n' between lines
    let match;
    if (match = line.match(/^(\w+)=(.*)$/)) {             // 
      section[match[1]] = match[2];
    } else if (match = line.match(/^\[(.*)\]$/)) {        // \[ == [        // \] == ]
      section = result[match[1]] = {};
    } else if (!/^\s*(;.*)?$/.test(line)) {               //   \s*: matches any whitespace character (equal to [\r\n\t\f\v])
      throw new Error("Line '" + line + "' is not valid.");
    }
  })
  return result;
}

console.log(parseINI(`
name=Vasilis
[address]
city=Tessaloniki`));
// ⯆ {name: "Vasilis", address: {…}}
//    ⯆ address:
//        city: "Tessaloniki"
//      ▶ __proto__: Object
//      name: "Vasilis"
//    ▶ __proto__: Object



// International characters
// =============
// by default, regular expression work on code units, as discussed in Chapter 5, not actual characters. This means characters that are composed of two code units behave strangely
console.log(/🍎{3}/.test("🍎🍎🍎"));          
console.log(/<.>/.test("🍎"));
console.log(/<.>/u.test("🍎"));
