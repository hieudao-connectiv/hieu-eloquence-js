console.log("numbers-again");

let number = /^[+\-]?(\d+(\.\d*)?|\.\d+)([eE][+\-]?\d+)?$/;
// /^...$: a whole string which doesn't whitespace (một chuỗi xuyên suốt)

// [+\-]?: \- is the character '-' literally; this regex string is it has or hasn't the '+|-' sign before a digit

// (\d+(\.\d*)?|\.\d+) 
  // \d+: able to have many number characters
  // (\.\d*)?
    // \. : is the dot (.) sign (decimal dot) literally
    // \d*: able to has or hasn't the number character
    // ? : has one time or hasn't a number character 
  // (\.\d+): after the dot sign has many number characters

// ([eE][+\-]?\d+)?   : has one time or hasn't the group of exponent notation
  // [eE][+\-]? : has one time or hasn't the the exponent notation (e+ or E+ or e- or E+)
  // \d+ : have many number characters after that

// Tests:
for (let str of ["1", "-1", "+15", "1.55", ".5", "5.", "1.3e2", "1E-4", "1e+12"]) {
  if (!number.test(str)) {
    console.log(`Failed to match '${str}'`);
  }
}
for (let str of ["1a", "+-1", "1.2.3", "1+1", "1e4.5", ".5.", "1f5", "."]) {
  if (number.test(str)) {
    console.log(`Incorrectly accepted '${str}'`);
  }
}

