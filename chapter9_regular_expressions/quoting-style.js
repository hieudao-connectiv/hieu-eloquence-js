let text = "'Today is the last Sunday of 2020', and 'tomorrow I'll have to go to work'";

console.log(text.replace(/(^|\W)'|'(\W|$)/g, '$1"$2'));
// → "Today is the last Sunday of 2020", and "tomorrow I'll have to go to work"

// (^|\W)': single quotation marks stand at the top of the line
// (^|\W)': or a non alphanumeric character is standing after the single quotation marks

// '(\W|$): a non alphanumeric character is standing before the single quotation marks
// '(\W|$): a non alphanumeric character is standing at the end of the string


