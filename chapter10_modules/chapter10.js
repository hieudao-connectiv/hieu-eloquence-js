// IMPROVISED MODULES
const weekDay = function() {
  const names = ["monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  return {
    names(number) {return names[number];},
    number(name) {return names.indexOf(name)}
  }
}();
console.log(weekDay.names(weekDay.number("Tuesday")));      // Tuesday 

// use the 'Function' constructor
let plushOne = Function("n", "return n + 1;");
console.log(plushOne(4));         // 5
// let plushOne = function("n", "return n + 1;");    // Uncaught SyntaxError: Unexpected string


// COMMONJS
const {formatDate} = require("./format_date");
console.log(formatDate(new Date(2017, 9, 13), "dddd the Do"));        // Friday the 13th


// MODULE DESIGN
var roads = [
  "Alice's House-Bob's House",   "Alice's House-Cabin",
  "Alice's House-Post Office",   "Bob's House-Town Hall",
  "Daria's House-Ernie's House", "Daria's House-Town Hall",
  "Ernie's House-Grete's House", "Grete's House-Farm",
  "Grete's House-Shop",          "Marketplace-Farm",
  "Marketplace-Post Office",     "Marketplace-Shop",
  "Marketplace-Town Hall",       "Shop-Town Hall"
];

function buildGraph(edges) {
  let graph = Object.create(null);
  function addEdge(from, to) {
    if (graph[from] == null) {
      graph[from] = [to];
    } else {
      graph[from].push(to);
    }
  }
  for (let [from, to] of edges.map(r => r.split("-"))) {
    addEdge(from, to);
    addEdge(to, from);
  }
  return graph;
}

var roadGraph = buildGraph(roads);

const {find_path} = require("dijkstrajs");

let graph = {};
for (let node of Object.keys(roadGraph)) {
  let edges = graph[node] = {};
  for (let dest of roadGraph[node]) {
    edges[dest] = 1;
  }
}

console.log(find_path(graph, "Post Office", "Cabin"));
// [ 'Post Office', "Alice's House", 'Cabin' ]