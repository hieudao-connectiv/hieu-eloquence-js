// #1
for (var i = 0; i <= 100; i++) {
  if (i % 3 == 0) {

    if (i % 5 == 0) {
      console.log("FizzBuzz");
    } else {
      console.log("Fizz");
    }

  } else if (i % 5 == 0) {
    console.log('Buzz');
  } else {
    console.log(i);
  }
}

// #2
for (let n = 0; n <=100; n++) {
  let result = "";
  if (n % 3 == 0) result += "Fizz";
  if (n % 5 == 0) result += "Buzz";
  console.log(result || n); // doesn't understand why!
}