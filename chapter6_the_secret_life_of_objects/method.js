let rabbit = {};
rabbit.speak = function(line) {
  console.log(`The rabbit says ${line}`);
}
rabbit.speak("I'm alive");

function speak(line) {
  console.log(`The ${this.type} rabbit says '${line}'`);
}
let whiteRabbit = {type: "white", speak};
whiteRabbit.speak("I'm here");

// Use a function's call method, which takes the 'this' value as the first argument and treats further arguments as normal parameters
speak.call(whiteRabbit, "Anybody see me?");
// method.js:8 The white rabbit says 'Anybody see me?'

// Call() with 'this' 
function normalize() {
  console.log(this.coords.map(n => n / this.length));
}
normalize.call({coords: [0,2,3], length: 5});   // (3) [0, 0.4, 0.6]
