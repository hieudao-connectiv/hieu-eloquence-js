let map = {one: true, two: true, hasOwnProperty: true};

// console.log(map);
// ⯆ {one: true, two: true, hasOwnProperty: true}
//      hasOwnProperty: true
//      one: true
//      two: true
//   ⯆ __proto__:
//       ▶ constructor: ƒ Object()
//       ▶ hasOwnProperty: ƒ hasOwnProperty()                   // it's this
//       ▶ isPrototypeOf: ƒ isPrototypeOf()
//       ▶ propertyIsEnumerable: ƒ propertyIsEnumerable()
//       ▶ toLocaleString: ƒ toLocaleString()
//       ▶ toString: ƒ toString()
//       ▶ valueOf: ƒ valueOf()
//       ▶ __defineGetter__: ƒ __defineGetter__()
//       ▶ __defineSetter__: ƒ __defineSetter__()
//       ▶ __lookupGetter__: ƒ __lookupGetter__()  
//       ▶ __lookupSetter__: ƒ __lookupSetter__()
//       ▶ get __proto__: ƒ __proto__()
//       ▶ set __proto__: ƒ __proto__()

console.log(Object.prototype.hasOwnProperty.call(map, 'one'));