class Group {
  constructor() {
    this.groups = [];

  }

  add(value) {
    if (this.groups.indexOf(value) == -1) {
      this.groups.push(value);
    }else {
      console.log('This value existed');
    }
  } 

  delete(value) {
    let index = this.groups.indexOf(value);
    if(index != -1) {
      this.groups.splice(index, 1);
      console.log(`The value: '${value}' was deleted in the array`);
    } else {
      console.log(`The value: '${value}' isn't exist in this array`);
    }
    return this.groups;
  }

  has(value) {
    let index = this.groups.indexOf(value);
    index != -1 ? console.log(true) : console.log(false);  
  }

  static from = function(collection) {
    let group = new Group();
    for (let i = 0; i < collection.length; i++) {
      group.add(collection[i]);
    }
    return group.groups;
  }
}

const newGroup = new Group();
newGroup.add('Dao');
newGroup.add('Trung');
newGroup.add('Hieu');
newGroup.add('Tau');
newGroup.add('Hieu');             // This value existed
newGroup.has('Hieu');             // true
newGroup.has('Hieuuu');           // false
newGroup.delete('Tau');           // The value: 'Tau' was deleted in the array
newGroup.delete('Tauuu');         // The value: 'Tauuu' isn't exist in this array
console.log(newGroup.groups);     // (3) ["Dao", "Trung", "Hieu"]

console.log(Group.from([1,2,3,4,5]));         // (5) [1, 2, 3, 4, 5]