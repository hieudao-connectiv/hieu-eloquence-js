let empty = {};
console.log(empty.toString);      // ƒ toString() { [native code] }
console.log(empty.toString());    // [object Object]
console.log(empty)                
// ⯆ {}
//   ⯆ __proto__:
//     ▶ constructor: ƒ Object()
//     ▶ hasOwnProperty: ƒ hasOwnProperty()
//     ▶ isPrototypeOf: ƒ isPrototypeOf()
//     ▶ propertyIsEnumerable: ƒ propertyIsEnumerable()
//     ▶ toLocaleString: ƒ toLocaleString()
//     ▶ toString: ƒ toString()
//     ▶ valueOf: ƒ valueOf()
//     ▶ __defineGetter__: ƒ __defineGetter__()
//     ▶  __defineSetter__: ƒ __defineSetter__()
//     ▶ __lookupGetter__: ƒ __lookupGetter__()
//     ▶ __lookupSetter__: ƒ __lookupSetter__()
//     ▶ get __proto__: ƒ __proto__()
//     ▶ set __proto__: ƒ __proto__()

let protoRabbit = {
  speak(line) {
    console.log(`The ${this.type} rabbit say '${line}'`);
  }
}

// Create an object with a specified prototype is pointed to an initial object
let killerRabbit = Object.create(protoRabbit);
killerRabbit.type = "Killer";
killerRabbit.speak("hi");       // The Killer rabbit say 'hi'
console.log(killerRabbit);
// ⯆ {type: "Killer"}
//      type: "Killer"
//   ⯆ __proto__:
//      ▶ speak: ƒ speak(line)
//      ▶ __proto__: Object