class Vec {
  constructor(width, height) {
    this.width = width;
    this.height = height;
  }

  plus(other) {
    return new Vec(this.width + other.width, this.height + other.height);
  }

  minus(other) {
    return new Vec(this.width - other.width, this.height - other.height);
  }

  get length() {
    return Math.sqrt(this.width * this.width + this.height * this.height);
  }
}

var rootVec = new Vec(3,4);

console.log(rootVec.plus(new Vec(5,6)));
// Vec {width: 8, height: 10}

console.log(rootVec.minus(new Vec(2,3)));
// Vec {width: 1, height: 1}

console.log(rootVec.length);      // 5