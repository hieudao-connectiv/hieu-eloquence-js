// In class-based object orientation programming, a "constructor" is a special type of subroutine called to create an object. It prepares an new object for use, often accepting arguments that the constructor uses to set required member variables

// Constructor in Javascript make sure an object as an instance of a given Class, has the properties that instances of this class are supposed to have

class Rabbit {
  constructor(type) {
    this.type = type;
  }

  speak(line) {
    console.log(`The ${this.type} rabbit says "${line}"`);
  }

}

console.dir(Rabbit);

let killerRabbit = new Rabbit("killer");
let blackRabbit = new Rabbit("black");

Rabbit.prototype.teeth = 'small';
blackRabbit.teeth = "big";

console.log(killerRabbit.teeth);    // small
console.log(blackRabbit.teeth);     // big




