// https://topdev.vn/blog/javascript-da-tao-ra-object-tu-function-nhu-the-nao/?fbclid=IwAR0tlR0wgZk4yKWIJuqggMQTDAvvAIIQjMlJFGKzKKrKZ_pFww_GL2JiHpc

// =================
// Object creation
// ===========

// 3 cách để tạo ra một object:

// Object as literal: Sử dụng cặp dấu ngoặc {} và bên trong đó là danh sách các property, method của object.

// Object.create: Sử dụng Object.create để tạo một object mới với __proto__ trỏ tới object ban đầu.

// Function constructor: Tạo object bằng việc sử dụng từ khóa new. Object mới sẽ có __proto__ trỏ tới prototype của function tạo ra nó.

//

// =================
// __proto__
// ===========

console.log("-----------------");
console.log("__proto__");

const dog = {
  name: "Puppy",
  run() {
    console.log(this.name + " is running...");
  },
};

console.log("dog: ");
console.log(dog);
// ⯆ {name: "Puppy", run: ƒ}
//      name: "Puppy"
//    ▶ run: ƒ run()
//        arguments: (...)
//        caller: (...)
//        length: 0
//        name: "run"
//      ▶ __proto__: ƒ ()       // property này trỏ về hàm gốc của JS ////// ▶ constructor: ƒ Function()  ()
//        [[FunctionLocation]]: 
//      ▶ [[Scopes]]: Scopes[2]
//  ▶ __proto__: Object   ==> "Object" ở đây là Object gốc của JS

// khi tạo ra một object từ "Object as literal" thì property "__proto__" của object mới sẽ luôn trỏ tới object gốc của JS

//

console.log("puppy: ");
let puppy = Object.create(dog);
console.log(puppy);
// ⯆ {}
//    ⯆__proto__:  ( from dog object)
//         name: "Puppy"
//       ⯆ run: f run()
//            arguments: (...)
//            caller: (...)
//            length: 0
//            name: "run"
//          ▶ __proto__: ƒ ()
//            [[FunctionLocation]]:
//          ▶ [[Scopes]]: Scopes[2]
//       ▶ __proto__: Object (JS)


// khi tạo ra một object từ một object khác bằng method "Object.create" thì property "__proto__" của object mới sẽ luôn trỏ tới object ban đầu (Object gốc của JS / hoặc object nó được khởi tạo từ), trừ khi chúng ta sử dụng Object.setPrototypeOf để bắt nó trỏ tới một object khác.

// __proto__ là một property mà mọi object trong JavaScript đều có. (bao gồm cả function)

//

// =================
// prototype
// ===========

console.log("-----------------");
console.log("prototype");


function Dog() {
  this.name = "bull";
}
Dog.prototype.run = function () {
  console.log(this.name + "is extremely handsome");
};

console.log("Dog: ");
console.dir(Dog);
// ⯆ ƒ Dog()
//      arguments: null
//      caller: null
//      length: 0
//      name: "Dog"
//    ▶ prototype: {run: f, constructor: ƒ}          // property này trỏ về hàm gốc là "f Dog()"
//       ▶ run: ƒ ()
//       ▶ constructor: ƒ Dog()
//       ▶ __proto__: Object
//    ▶ __proto__: ƒ ()                              // property này trỏ về hàm gốc của JS
//       [[FunctionLocation]]: VM4346:1
//    ▶ [[Scopes]]: Scopes[2]

// Khi tạo ra function mới "Dog" thì property "prototype" đã được tạo sẵn (trong Object k có thuộc tính này)
// "prototype": nơi để ta xây dựng các nguyên mẫu và sau đó đem nhân bản ra các object khác

// ====> Conclusion: mọi object được tạo ra bằng keyword new sẽ có '__proto__' (từ JS) trỏ tới 'prototype' của đối tượng đã tạo ra nó (trong trường hợp trên là function Dog). (chú ý gạch ngang đơn)


console.dir(Dog.prototype);
// ----------------
// ⯆ Object
//    ▶ run: ƒ ()
//    ▶ constructor: ƒ Dog()
//    ▶ __proto__: Object
// -----------------

console.log(Dog.prototype);
// ⯆ {run: ƒ, constructor: ƒ}
//    ▶ run: ƒ ()
//    ▶ constructor: ƒ Dog()
//    ▶ __proto__: Object

const dogg = new Dog();
console.log("dog: ");
console.log(dogg);
// Dog {name: "hieu"}
// name: "hieu"
// __proto__: Object
// ------------
// run: ƒ ()
// constructor: ƒ Dog()
// __proto__: Object
// --------------
console.log(dogg.prototype); // undefined


