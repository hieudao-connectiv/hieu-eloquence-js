// let ages = {
//   Boris: 39,
//   Liang: 22,
//   Julia: 23
// }

// console.log(`Julia is ${ages['Julia']} ages`);
// // Julia is 23 ages

// console.log('Is Jacks age known?', 'Jack' in ages);
// // Is Jacks age known false

// console.log('Is Boris age known?', 'Boris' in ages, `${ages['Boris']}`);
// // Is Boris age known? true 39

// console.log('Is toStrings age known?', 'toString' in ages);
// // Is toStrings age known? true
// // ---------
// // Why "toString" in ages is true???
// // Because plain objects derive from Object.prototype, it looks like the property is there. As such, using plain objects as maps is dangerous.


// // To avoid this problem, first, it is possible to create objects with no prototype.
// // If you pass 'null' to Object.create, the resulting object will no derive from Object.prototype and can safely be used as a map.
// let ObjectWithoutPrototype = Object.create(null);
// console.dir(ObjectWithoutPrototype);
// // ⯆ Object
// //    No properties

// console.log('toSring' in ObjectWithoutPrototype);   // false


//////////////////////////////

////////// ===== ///////////
// =====  Map
////////// ===== ///////////

// The 'Map' object holds key-value pairs and remembers the original insertion order of the keys.

// let tall = new Map();
// tall.set("hieu", 178);
// tall.set("Jem", 198);
// tall.set("David", 108);

// console.log(tall);
// ⯆ Map(3) {"hieu" => 178, "Jem" => 198, "David" => 108}
//    ⯆ [[Entries]]
//       ▶ 0: {"hieu" => 178}
//       ▶ 1: {"Jem" => 198}
//       ▶ 2: {"David" => 108}
//        size: (...)
//    ▶ __proto__: Map


// console.log(`Hieu is tall ${tall.get('hieu')}`);
// // Hieu is tall 178

// console.log("Is Jem's tall know?", tall.has('Jem'));
// // Is Jem's tall know? true

// console.log(tall.has('toString'));
// // false



//////////////////////////
// console.log("//////////////////////////");



// let a = { 'yname': 'hieu' };
// let b = { 'sex': 'male' }
// let c = { true: 'true' }
// let me = {};

// // Object

// me[a] = 'daotrunghieu';       // a: [object Object]
// // console.log(me['a']);         // undefined
// // console.log(me[a]);           // daotrunghieu
// // console.log(me.a);            // undefined

// // me['a'] = 'daotrunghieu';      
// // console.log(me[a]);            // undefined
// // console.log(me['a']);          // daotrunghieu
// // console.log(me.a);             // daotrunghieu

// // me.a = 'daotrunghieu';     
// // console.log(me['a']);          // daotrunghieu
// // console.log(me[a]);            // undefined
// // console.log(me.a);             // daotrunghieu

// me[b] = 'daohieu';            // b: [object Object]
// // vì a và b ở cùng 1 dạng key và value như nhau nên nó sẽ bị ghi đè kết quả lên nhau. thành ra console.log sẽ chỉ ra 'daohieu'. 
// // can't use object as keys for other objects

// console.log(me[b]);             // daohieu
// console.log(me[a]);             // daohieu

// me[c] = 'my homework';

// console.log(me[a]);             // my homework
// console.log(me[b]);             // my homework
// console.log(me[c]);             // my homework

// let join = { yname: 'hieu' };
// let newObject = {};
// newObject[join] = 123;
// console.log(newObject['[object Object]']);    // 123
// // Nếu muốn sử dụng object như keys thì phải truy cập keys như vậy

// // ======
// // MAP
// // ======
// let characters = new Map();
// characters.set(a, 'first');           // typeof 'key: a' is object
// characters.set(b, 'second');          // typeof 'key: b' is object

// // Key field: in Object, it follows the rule of normal dictionary. The keys MUST be simple types — either integer or string or symbols. Nothing more. But in Map it can be any data type (an object, an array, etc…). (try using another object as Object’s property key — i dare you :))

// characters.set(true, 'true');         // typeof 'key: true' is boolean
// characters.set(false, 'false');       // typeof 'key: false' is boolean
// characters.set(1, 'number 1');        // typeof 'key: 1' is number
// characters.set('1', 'string 1');      // typeof " key: '1' " is boolean

// console.log(characters.get(a));     // first
// console.log(characters.get(b));     // second
// console.log('\n');

// for (var [key, value] of characters) {
//   console.log("typeof key: "  +  typeof key);
//   console.log(key + " = " + value);
//   console.log("\n");
// }

// // Object.entries: Map from Object
// //If we have a plain object, and we’d like to create a Map from it, then we can use built-in method Object.entries(obj) that returns an array of key/value pairs for an object exactly in that format.
// // Here, Object.entries returns the array of key/value pairs: [ ["yname","John"], ["age", 30] ]. That’s what Map needs.

// let obj = {
//   yname: 'hieu',
//   age: 22,
//   sex: 'male'
// }

// let map = new Map(Object.entries(obj));
// console.log(map);
// // ⯆ Map(3) {"yname" => "hieu", "age" => 22, "sex" => "male"}
// //  ⯆[[Entries]]
// //    ▶ 0: {"yname" => "hieu"}
// //    ▶ 1: {"age" => 22}
// //    ▶ 2: {"sex" => "male"}
// //      size: (...)
// //    ▶ __proto__: Map

// console.log(Object.entries(obj));  // return a new Array :))
// // ⯆ (3) [Array(2), Array(2), Array(2)]
// //    ▶ 0: (2) ["yname", "hieu"]
// //    ▶ 1: (2) ["age", 22]
// //    ▶ 2: (2) ["sex", "male"]
// //      length: 3
// //    ▶ __proto__: Array(0)


// console.log(typeof map); // object


// // Object.fromEntries: Object from Map
// let prices = Object.fromEntries([
//   ['banana', 1],
//   ['orange', 2],
//   ['meat', 3],
// ]);
// console.log(typeof prices)  // Object
// console.log(prices);
// ⯆ Object
//     banana: 1
//     meat: 3
//     orange: 2
//   ▶ __proto__: Object



//////////////////////
console.log("//////////////////////")



// var myMap = new Map();

// var yname = 'a string',
//   age = {},
//   sex = function () { };

// console.log('age: '  + typeof age);                 // age: object
// console.log('yname: ' + myMap.get(yname));          // yname: undefined
// console.log('sex: ' + myMap.get( sex));             // sex: undefined

// myMap.set('yname', 'hieu');
// myMap.set('yname', 'Jame');       // will be overwritten
// myMap.set(yname, 'Javascript');

// myMap.set('age', '22');
// myMap.set(age, {});               // like this: {} => {};

// myMap.set('sex', 'male');
// myMap.set('sex', 'female');       // will be overwritten

// console.log(myMap);
// // ⯆ Map(5) {"yname" => "Jame", "a string" => "Javascript", "age" => "22", {…} => {…}, "sex" => "female"}
// //    ⯆ [[Entries]]
// //      ▶ 0: {"yname" => "Jame"}
// //      ▶ 1: {"a string" => "Javascript"}
// //      ▶ 2: {"age" => "22"}
// //      ▶ 3: {Object => Object}
// //      ▶ 4: {"sex" => "female"}
// //        size: 5
// //    ▶ __proto__: Map

// console.log(myMap.size);                // 5

// console.log(myMap.get('sex'));          // female
// console.log(myMap.get('age'));          // 22
// console.log(myMap.get(age));            // ⯆ {}  ▶ __proto__: Object

// console.log(myMap.get('yname'));        // Jame
// console.log(myMap.get(yname));          // Javascript

// console.log(myMap.get('a string'));     // Javascript

// console.log('age: ' + myMap.get(age));                    // age: [object Object]
// console.log('yname: ' + myMap.get(yname));                // yname: Javascript
// console.log('sex: ' + myMap.get(function () { }));        // sex: [object Object]



//////////////////////
console.log("//////////////////////")



// Using NaN as Map keys
var myMap = new Map();
myMap.set(NaN, 'not a number');
console.log(myMap.get(NaN));                // not a number

var otherNaN = Number('foo');
console.log(otherNaN);                      // NaN
console.log(myMap.get(otherNaN));           // not a number
console.log('\n');

// Iterating Map with for..of
var mMap = new Map();
mMap.set('yname', 'hieu');
mMap.set('yname',);                         // will be overwritten
mMap.set('brand', 'honda');
mMap.set('This is', 'my', 'first time');    // the third parameter will be get rid
mMap.set(2, 'KTM');
mMap.set(3, 'NVX');


// for (var [key, value] of mMap) {
//   console.log(key + ' = ' + value);
// }
// yname = undefined
// brand = honda
// This is = my
// 2 = KTM
// 3 = NVX

// The entries() method returns a new Iterator object that contains the [key, value] pairs for each element in the Map object in insertion order

// console.log(mMap.entries());
// ⯆ MapIterator {"yname" => undefined, "brand" => "honda", "This is" => "my", 2 => "KTM", 3 => "NVX"}
//    ⯆ [[Entries]]
//       ▶ 0: {"yname" => undefined}
//       ▶ 1: {"brand" => "honda"}
//       ▶ 2: {"This is" => "my"}
//       ▶ 3: {2 => "KTM"}
//       ▶ 4: {3 => "NVX"}
//    ▶ __proto__: Map Iterator
//      [[IteratorHasMore]]: true
//      [[IteratorIndex]]: 0
//      [[IteratorKind]]: "entries"

// for (var [key, value] of mMap.entries()) {
//   console.log(key + ' = ' + value);
// }

console.log('\n');

// Using the keys() or value() method returns a new Iterator object that contains the keys or values of each element in the mMap object in insertion order
console.log(typeof mMap.keys());            // object

// for (var key of mMap.keys()) {
//   console.log('key: ' + key);
// }

// for (var value of mMap.values()) {
//     console.log('key: ' + value);
// }

// for (var elem of mMap.entries()) {
//   console.log(elem);
// }
// (2) ["yname", undefined]
// (2) ["brand", "honda"]
// (2) ["This is", "my"]
// (2) [2, "KTM"]
// (2) [3, "NVX"]


// mMap.forEach(function(value, key) {
//   console.log(key + ' = ' + value);
// }) 
// yname = undefined
// brand = honda
// This is = my
// 2 = KTM
// 3 = NVX


////////////////////


// Relation with Array objects
var kvArray = [['key1', 'value1'], ['key2', 'value2'], ['key3', 'value3']];
var myMap = new Map(kvArray);

console.log(myMap.get('key1'));              // value1
console.log(Array.from(myMap));
// ⯆ (3) [Array(2), Array(2), Array(2)]
//    ▶ 0: (2) ["key1", "value1"]
//    ▶ 1: (2) ["key2", "value2"]
//    ▶ 2: (2) ["key3", "value3"]
//      length: 3
//    ▶ __proto__: Array(0)

console.log([...myMap]);                    // (3) [Array(2), Array(2), Array(2)] 
console.log(Array.from(myMap.keys()));      // (3) ["key1", "key2", "key3"]

// Cloning and merging Maps
var original = new Map([[1, 'one']]);
console.log(original);
// ⯆ Map(1) {1 => "one"}
//    ⯆ [[Entries]]
//      ▶ 0: {1 => "one"}
//      ▶ size: (...)
//    ▶ __proto__: Map
console.log(original.get(1));               // one

var clone = new Map(original);
console.log(clone);
// ⯆ Map(1) {1 => "one"}
//    ⯆ [[Entries]]
//      ▶ 0: {1 => "one"}
//      ▶ size: (...)
//    ▶ __proto__: Map

console.log(clone.get(1));                  // one
console.log(original == clone);             // false
console.log(original === clone);            // false

// Maps can be merged, maintaining key uniqueness:
var first = new Map([[1, 'one'], [2, 'two'], [3, 'three']]);
var second = new Map([[1, 'mot'], [2, 'hai'], [3, 'ba']]);

var merged = new Map([...first, ...second]);
console.log(merged);
// ⯆ Map(3) {1 => "mot", 2 => "hai", 3 => "ba"}
//    ⯆ [[Entries]]
//      ▶ 0: {1 => "mot"}
//      ▶ 1: {2 => "hai"}
//      ▶ 2: {3 => "ba"}
//        size: 3
//    ▶ __proto__: Map

console.log(merged.get(1));       // mot

var merged = new Map([...second, ...first]);
console.log(merged);
// ⯆ Map(3) {1 => "one", 2 => "two", 3 => "three"}
//    ⯆ [[Entries]]
//      ▶ 0: {1 => "mot"}
//      ▶ 1: {2 => "hai"}
//      ▶ 2: {3 => "ba"}
//        size: 3
//    ▶ __proto__: Map

console.log(merged.get(1));       // one

// Maps can be merged with Arrays, too
var dautien = new Map([[1, 'mot'], [2, 'hai'], [3, 'ba']]);
var thuhai = new Map([[4, 'one'], [5, 'two'], [6, 'three']]);

var gop = new Map([...dautien, ...thuhai, ['con co', 'con vac']]);
console.log(gop);
// ⯆ Map(7) {1 => "mot", 2 => "hai", 3 => "ba", 4 => "one", 5 => "two", …}
//    ⯆ [[Entries]]
//       ▶ 0: {1 => "mot"}
//       ▶ 1: {2 => "hai"}
//       ▶ 2: {3 => "ba"}
//       ▶ 3: {4 => "one"}
//       ▶ 4: {5 => "two"}
//       ▶ 5: {6 => "three"}
//       ▶ 6: {"con co" => "con vac"}
//       size: 7
//    ▶ __proto__: Map

for (var element of gop) {
  console.log(element);
}
// (2) [1, "mot"]
// (2) [2, "hai"]
// (2) [3, "ba"]
// (2) [4, "one"]
// (2) [5, "two"]
// (2) [6, "three"]
// (2) ["con co", "con vac"]